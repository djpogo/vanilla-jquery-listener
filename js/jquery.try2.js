/*global jQuery*/
jQuery(function ($) {
    "use strict";
    $(".viewportChanger").on("click", function () {
        $(this).toggleClass("show");
    });
});
