/*global jQuery*/
jQuery(function ($) {
    "use strict";
    $(".viewportChanger").click(function () {
        $(this).toggleClass("show");
    });
});
