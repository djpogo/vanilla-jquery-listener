/*global window, document*/
(function () {
    "use strict";
    
    function toggleClass(el) {
        var x = el.target;
        if (x.classList.contains("show")) {
            x.classList.remove("show");
        } else {
            x.classList.add("show");
        }
    }
    
    function domReady(event) {
        var els = document.querySelectorAll(".viewportChanger"),
            elsCount = els.length,
            i;
        for (i = 0; i < elsCount; ++i) {
            els[i].addEventListener("click", toggleClass);
        }
    }

    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", function () {
            document.removeEventListener("DOMContentLoaded", undefined, false);
            domReady();
        }, false);
        
    // If IE event model is used
    } else if (document.attachEvent) {
    // ensure firing before onload
        document.attachEvent("onreadystatechange", function () {
            if (document.readyState === "complete") {
                document.detachEvent("onreadystatechange", undefined);
                domReady();
            }
        });
    }
}());
