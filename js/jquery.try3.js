/*global jQuery*/
jQuery(function ($) {
    "use strict";
    $(".viewport").on("click", ".viewportChanger", function () {
        $(this).toggleClass("show");
    });
});
