/*global $$*/
(function () {
    "use strict";
    $$(".viewport").addEvent("click:relay(.viewportChanger)", function (event, clicked) {
        clicked.toggleClass("show");
    });
}());