<ul class="viewport">
    <?php 
        $amount = filter_input(INPUT_GET, 'amount', FILTER_VALIDATE_INT);
        if (!$amount) {
            $amount = 10000;   
        }
    ?>
    <?php for($i = 0; $i < $amount; ++$i): ?>
    <li class="viewportChanger">Item #<?=$i+1?></li>
    <?php endfor; ?>
</ul>
